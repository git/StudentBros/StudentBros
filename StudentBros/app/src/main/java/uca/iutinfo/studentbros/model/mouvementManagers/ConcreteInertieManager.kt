package uca.iutinfo.studentbros.model.mouvementManagers

import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.gameManagers.ConcreteCollisionManager
import uca.iutinfo.studentbros.model.gameManagers.MouvementManager
import kotlin.concurrent.thread

class ConcreteInertieManager(val mouvementManager: MouvementManager) : InertieManager {


    var gottaMoveEmAll = mutableMapOf<Element, Float>()
    var inertie = 5F

    override fun moveElement(player: Player, level: Level) {
        gottaMoveEmAll[player] = player.velX
    }

    override fun moveElement(element: Element, level: Level) {
        gottaMoveEmAll[element] = element.velX
    }

    override fun onChanged(t: GameLoop?) {
        val ite = gottaMoveEmAll.iterator()
        try {
            ite.forEach {
                var result = false
                thread {
                    if (t!=null) {
                        if (it.key is Player) result = mouvementManager.moveP(it.key as Player, t.level, it.value*t.screenWidthCoef, 0F)
                        else mouvementManager.moveE(it.key, t.level, it.value*t.screenWidthCoef, 0F)
                        var distance = 5F
                        if (it.value > 0) distance = -inertie
                        if (result && ((it.value > 0F && it.value + distance >= 0F) || (it.value<0F && it.value+ distance <= 0F))) {
                            it.setValue(it.value+ distance)
                        } else {
                            gottaMoveEmAll[it.key] = 0F
                        }
                    }
                }
            }
        }catch (e: ConcurrentModificationException){
            println("oops")
        }

    }
}