package uca.iutinfo.studentbros.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import uca.iutinfo.studentbros.factories.fragment_creator
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = fragment_creator(/*model*/)
        super.onCreate(savedInstanceState)
    }
}