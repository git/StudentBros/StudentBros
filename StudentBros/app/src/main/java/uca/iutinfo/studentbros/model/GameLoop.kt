package uca.iutinfo.studentbros.model

import android.database.Observable
import androidx.lifecycle.Observer
import uca.iutinfo.studentbros.model.gameManagers.GameManager
import kotlin.concurrent.thread

class GameLoop(val level: Level, var screenWidthCoef: Float, var screenHeightCoef: Float, var screenWidth: Float) : Observable<GameManager>(), ElementObserver {

    var running : Boolean = true

    fun gameThread(){
        thread{
            while (running){
                Thread.sleep(30L)
                notif()
            }
        }
    }

    fun addObserver(manager: GameManager){
        mObservers.add(manager)
    }

    private fun notif(){
        mObservers.forEach { it.onChanged(this) }
    }

    override fun notreOnChanged(victoire: Boolean) {
        running=false
    }

}