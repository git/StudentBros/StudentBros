package uca.iutinfo.studentbros.model.gameManagers

import uca.iutinfo.studentbros.model.Element
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Player
import uca.iutinfo.studentbros.model.Side

interface MouvementManager {

    fun moveAllElements(level: Level)

    fun moveE(element: Element, level: Level, x: Float, y: Float): Boolean

    fun moveP(player: Player, level: Level, x: Float, y: Float): Boolean

    fun moveEFromPlayer(element: Element, level: Level, x: Float)
    fun movePlayer(player: Player, level: Level, side: Side, screenWidhtCoef: Float, screenHeightCoef: Float)
}