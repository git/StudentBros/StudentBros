package uca.iutinfo.studentbros.model.mouvementManagers

import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.gameManagers.CollisionManager
import uca.iutinfo.studentbros.model.gameManagers.MouvementManager
import kotlin.concurrent.thread

class ConcreteGravityManager(val mouvementManager: MouvementManager, val collisionManager: CollisionManager) : GravityManager {

    var fallingBoys = mutableMapOf<Element, Float>()

    val gravity = -7.5F

    override fun jump(element: Element, level: Level) {
        fallingBoys[element] = element.velY
    }

    override fun makeThemFall(level: Level) {
        level.elements.filter { it.gravityAffects && it.isVisible && it.right > 0 && !collisionManager.touchingGrass(it, level) && !fallingBoys.containsKey(it)}.forEach { fallingBoys[it] = 0F }
    }

    override fun onChanged(t: GameLoop?) {
        try {
            thread {
                if (t != null) {
                    makeThemFall(t.level)
                }
            }
            val ite = fallingBoys.iterator()
            ite.forEach {
                thread {
                    if (t != null) {
                        val result = mouvementManager.moveE(
                            it.key,
                            t.level,
                            0F,
                            it.value * t.screenHeightCoef
                        )
                        if (result) {
                            it.setValue(it.value + gravity)
                        } else if (it.value > 0) it.setValue(gravity)
                        else fallingBoys[it.key] = 0F
                    }
                }
            }
        } catch (e: ConcurrentModificationException){
            println("oops")
        }
    }


}