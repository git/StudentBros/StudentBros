package uca.iutinfo.studentbros.multiplayer

import android.util.Log
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

object SocketHandler {

    lateinit var mSocket : Socket

    @Synchronized
    fun setSocket(){
        try{
            mSocket=IO.socket("https://codefirst.iut.uca.fr/containers/StrudentBros-server-studentBros")
        }catch (e: URISyntaxException){
            Log.d("hey", "uri pas bon")
        }
    }

    @Synchronized
    fun getSocket(): Socket{
        return mSocket
    }
}