package uca.iutinfo.studentbros.model

import kotlin.concurrent.thread

class StandardPowerUp(left: Float, bottom: Float, parent: Level, velX: Float, velY: Float) : PowerUp(left+100, left, bottom+100, bottom, parent, velX, velY, false, true, emptyMap(), "", listOf(), "StandardPowerUp")  {
    override fun doSomething(side: Side, screenWidthCoef: Float, screenHeightCoef: Float, player: Player) {
        return
    }

    override fun lost(p: Player) {
        return
    }

    override fun onHit(p: Player, e: Element) {
        if (!p.isImmortal){
            p.vies--
            if(p.vies == 0) {
                p.mourir()
                return
            }
            p.isImmortal = true
            thread(start=true) {
                Thread.sleep(1000)
                p.isImmortal = false
            }
        }
    }

    override fun actionOnCollision(e: Element, s: Side) {

    }

    override fun jump() {
        return
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
        return
    }

    override fun increaseLeftVelocity() {
        return
    }

    override fun increaseRightVelocity() {
        return
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if(e is Player){
            e.pickPowerUp(this)
            this.mourir()
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
        this.isVisible = false
    }

    override fun randomAction() {
    }
}