package uca.iutinfo.studentbros.model

abstract class PowerUp(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float=0F, velY: Float=0F, isImmortal: Boolean = false, isVisible: Boolean = true, assets: Map<String, String>, selectedAsset: String, dangerousSides: List<Side>, val name: String) : Element(right, left, top, bottom, parent, velX, velY, isImmortal, isVisible, assets, selectedAsset, dangerousSides, false) {
    abstract fun doSomething(side: Side, screenWidthCoef: Float, screenHeightCoef: Float, player: Player)
    abstract fun lost(p: Player)
    abstract fun onHit(p: Player, e: Element)
}