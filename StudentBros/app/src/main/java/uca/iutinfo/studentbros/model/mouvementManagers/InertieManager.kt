package uca.iutinfo.studentbros.model.mouvementManagers

import androidx.lifecycle.Observer
import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.gameManagers.GameManager

interface InertieManager : GameManager {

    fun moveElement(player: Player, level: Level)

    fun moveElement(element: Element, level: Level)


}