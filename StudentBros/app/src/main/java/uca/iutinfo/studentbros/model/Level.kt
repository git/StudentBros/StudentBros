package uca.iutinfo.studentbros.model

import java.io.Serializable

class Level(val numero: Int, val name: String, val elements: MutableList<Element> = mutableListOf()):
    Serializable {

    fun addElement(element: Element){
        elements.add(element)
    }

    fun removeElements(){
        val iterator = elements.listIterator()
        iterator.forEach { if(!it.isVisible || it.right<0) iterator.remove()}
    }

    fun changeSize(widthCoef: Float, heightCoef: Float){
        elements.forEach {
            it.bottom*=heightCoef
            it.top*=heightCoef
            it.left*=widthCoef
            it.right*=widthCoef
        }
    }

    fun getEndPos(): Float? = elements.find { it is EndFlag }?.left
}