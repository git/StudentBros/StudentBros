package uca.iutinfo.studentbros.model

class SpikyBlock(right: Float, left: Float, bottom: Float, top: Float, parent: Level, velX: Float=0F, velY: Float=0F) : Block(right, left, top, bottom, parent, velX, velY, emptyMap(), "Blocks/SpikyBlocks.png", listOf(Side.LEFT, Side.RIGHT, Side.DOWN, Side.UP)) {
    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }

    override fun prendreUnCoup(e: Element, side: Side) {
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
    }

    override fun randomAction() {
    }

    override fun actionOnCollision(e: Element, s: Side) {
        if(!this.dangerousSides.filter { it -> it == s }.isEmpty()){
            if(!this.isImmortal) this.prendreUnCoup(e, s.inverse())
        } else {
            this.mettreUnCoup(e)
        }
    }
}