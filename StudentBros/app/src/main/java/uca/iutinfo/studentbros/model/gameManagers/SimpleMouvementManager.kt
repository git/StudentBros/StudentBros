package uca.iutinfo.studentbros.model.gameManagers

import uca.iutinfo.studentbros.model.*

class SimpleMouvementManager(val screenCenter: Float) : MouvementManager {

    override fun moveAllElements(level: Level) {
        level.elements.filterNot { it is Player || !it.isVisible }.forEach { moveE(it, level, it.velX, it.velY) }
    }

    override fun moveE(element: Element, level: Level, x: Float, y: Float): Boolean {
        element.top += y
        element.bottom += y
        element.right += x
        element.left += x
        return true
    }

    override fun moveP(player: Player, level: Level, x: Float, y: Float): Boolean {
        if (player.right<screenCenter){
            var x = player.velX
            if (player.right + player.velX > screenCenter){
                x = screenCenter - player.right
                level.elements.filterNot { it is Player || !it.isVisible }.forEach{ moveEFromPlayer(it, level, -x)}
            }
            else if (player.left + player.velX < 0){
                x = -player.left
            }
            player.top += y
            player.bottom += y
            player.right += x
            player.left += x
        }
        else if (player.velX>0){
            player.top += y
            player.bottom += y
            player.right += x
            player.left += x
            level.elements.filterNot { it is Player || !it.isVisible  }.forEach{ moveEFromPlayer(it, level, -player.velX)}
        }
        else{
                player.top += y
                player.bottom += y
                player.right += x
                player.left += x
        }
        return true
    }

    override fun movePlayer(player: Player, level: Level, side: Side, screenWidhtCoef: Float, screenHeightCoef: Float) {
        when(side){
            Side.LEFT -> moveP(player, level, -40F, 0F)
            Side.RIGHT -> moveP(player, level, 40F, 0F)
            Side.UP -> moveP(player, level, 0F, 40F)
            Side.DOWN -> moveP(player, level, 0F, -40F)
        }
    }

    override fun moveEFromPlayer(element: Element, level: Level, x: Float) {
        element.right += x
        element.left += x
    }


}