package uca.iutinfo.studentbros.model

class EndFlag(right: Float, left: Float, top: Float, bottom: Float, parent: Level) : Element(right, left, top, bottom, parent, 0F, 0F, true, true, emptyMap(), "selectedAsset", emptyList(), false) {
    override fun actionOnCollision(e: Element, s: Side) {
        prendreUnCoup(e, s)
    }

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if(e is Player){
            notifyEnd()
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
    }

    override fun randomAction() {
    }

    fun notifyEnd() {
        mObservers.forEach { it.notreOnChanged(true) }
    }

    fun addObserver(e: ElementObserver) {
        mObservers.add(e)
    }

}