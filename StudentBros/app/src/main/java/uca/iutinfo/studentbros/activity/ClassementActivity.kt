package uca.iutinfo.studentbros.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uca.iutinfo.studentbros.R
import uca.iutinfo.studentbros.data.Stub
import uca.iutinfo.studentbros.views.AdapterLevelChoice


class ClassementActivity : AppCompatActivity(){

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            setContentView(R.layout.layout_classement)

        }

        override fun onResume() {
            super.onResume()
            val scores = Stub().getScore();
            //findViewById<RecyclerView>(R.id.recyclerViewLevelChoice).adapter = AdapterLevelChoice(scores)
            findViewById<RecyclerView>(R.id.recyclerViewLevelChoice).layoutManager = LinearLayoutManager(this)
        }
    }
