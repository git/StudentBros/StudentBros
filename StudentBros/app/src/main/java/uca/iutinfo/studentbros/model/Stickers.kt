package uca.iutinfo.studentbros.model

import uca.iutinfo.studentbros.model.powerups.PlayerThrowableElement
import kotlin.concurrent.thread


class Stickers(left: Float, bottom: Float, parent: Level, velX: Float, velY: Float) : Enemy(left+150, left, bottom+200, bottom, parent, velX, velY, false, true,  mapOf("TurtleLeft" to "Stickers/TurtleLeft.png", "TurtleRight" to "Stickers/TurtleRight.png", "RunningTurtleLeft" to "Stickers/RunningTurtleLeft.png", "RunningTurtleRight" to "Stickers/RunningTurtleRight.png"), "Stickers/RunningTurtleRight.png", listOf(Side.RIGHT, Side.LEFT, Side.DOWN), true, 1) {


    var run = 1
    override fun actionOnCollision(e: Element, s: Side) {
        if(e.dangerousSides.any{ it.inverse() == s }){
            if(!this.isImmortal){
                this.prendreUnCoup(e, s.inverse())
                e.bounce()
            }
        } else {
            e.prendreUnCoup(this, s.inverse())
        }
        if(velX >= 0) increaseLeftVelocity()
        else if(velX < 0) increaseRightVelocity()
    }

    override fun jump() {
        this.velY = 60F
    }

    override fun bounce() {
        this.velY = 100F
    }

    override fun increaseDownVelocity() {
        this.velY = 0F
    }

    override fun increaseLeftVelocity() {
        this.velX = -20F
        if (run == 1){
            selectedAsset = assets.filter { it.key == "RunningTurtleLeft" }.values.first()
            run = 2
        }
        else{
            selectedAsset = assets.filter { it.key == "TurtleLeft" }.values.first()
            run = 1
        }
    }

    override fun increaseRightVelocity() {
        this.velX = 20F
        if (run == 1){
            selectedAsset = assets.filter { it.key == "RunningTurtleRight" }.values.first()
            run = 2
        }
        else{
            selectedAsset = assets.filter { it.key == "TurtleRight" }.values.first()
            run = 1
        }
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if(e is Player || e is PlayerThrowableElement) {
            vies--
            if(vies == 0) {
                mourir()
                return
            }
            isImmortal = true
            thread {
                Thread.sleep(1500L)
                isImmortal = false
            }
        }
    }

    override fun mettreUnCoup(e: Element) {
        //e.prendreUnCoup(this)
    }

    override fun mourir() {
        isVisible = false
        //TOTO animation de mort
    }

    override fun randomAction() {
        if(velX >= 0){
            increaseRightVelocity()
        }
        if(velX < 0){
            increaseLeftVelocity()
        }
    }
}