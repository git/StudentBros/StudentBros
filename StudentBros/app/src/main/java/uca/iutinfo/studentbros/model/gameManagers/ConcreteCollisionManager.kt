package uca.iutinfo.studentbros.model.gameManagers

import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.powerups.FireBall
import uca.iutinfo.studentbros.model.powerups.ThrowableElement

class ConcreteCollisionManager : CollisionManager {

    override fun detectCollision(element: Element, level: Level, x:Float, y: Float): Boolean {
        level.elements.forEach { if (it.isVisible && element.intersectsWithVelocity(it, x, y)) return true }
        return false
    }

    override fun distanceToCollision(element: Element, level: Level, x:Float, y: Float): Pair<Float, Float> {
        var distances = Pair(x, y)
        var laColision: Element? = null
        var elSide: Side? = null


        level.elements.filterNot { it==element || !it.isVisible || !element.isVisible }.forEach {
            if (element.intersectsWithVelocity(it, x, y)){
                val pair = getCollisionDistances(element, it, distances)
                val side = getCollisionSide(element, it, pair)
                distances = pair
                elSide=side
                laColision=it
            }
        }
        if (element.top+distances.second<=0F){
            element.mourir()
        }
        var col = laColision
        var side = elSide
        if (col != null && side != null){
            collisionAction(element, col, side)
        }
        return distances
    }

    override fun collisionAction(element: Element, elementHitted: Element, collisionSide: Side) {
        element.actionOnCollision(elementHitted, collisionSide)
    }

    override fun getCollisionSide(element: Element, elementHitted: Element, pair: Pair<Float, Float>): Side {
        val x = pair.first
        val y = pair.second
        if (x>0) return Side.RIGHT
        if (x<0) return Side.LEFT
        if (y>0) return Side.UP
        return Side.DOWN
    }

    override fun getCollisionDistances(element: Element, elementHitted: Element, pair: Pair<Float, Float>): Pair<Float, Float> {
        var x = pair.first
        var y = pair.second
        if (x>0 && element.right + x > elementHitted.left && element.right <= elementHitted.left) x = elementHitted.left - element.right
        if (x<0 && element.left + x < elementHitted.right && element.left >= elementHitted.right) x = elementHitted.right - element.left
        if (y>0 && element.top + y > elementHitted.bottom && element.top <= elementHitted.bottom) y = elementHitted.bottom - element.top
        if (y<0 && element.bottom + y < elementHitted.top && element.bottom >= elementHitted.top) y = elementHitted.top - element.bottom

        return Pair(x, y)
    }

    override fun touchingGrass(element: Element, level: Level): Boolean {
        level.elements.filter { it!=element && it.isVisible }.forEach {
            if (it.top == element.bottom && ((element.right<it.right && element.right >it.left) || (element.left > it.left && element.left < it.right))) {
                return true
            }
        }
        return false
    }

    override fun directCollision(element: Element, level: Level, side: Side): Boolean {
        level.elements.filter { it != element && it.isVisible}.forEach {
            when(side){
                Side.UP -> if (it.bottom == element.top && ((element.right<=it.right && element.right >=it.left) || (element.left >= it.left && element.left <= it.right))) return true
                Side.DOWN -> if (it.top == element.bottom && ((element.right<=it.right && element.right >=it.left) || (element.left >= it.left && element.left <= it.right))) return true
                Side.RIGHT -> if (it.left == element.right && ((element.top<=it.top && element.bottom >= it.bottom) || (element.bottom >= it.bottom && element.top <= it.top))) return true
                Side.LEFT -> if (it.right == element.left && ((element.top<=it.top && element.bottom >= it.bottom) || (element.bottom >= it.bottom && element.top <= it.top))) return true
            }
        }
        return false
    }

}