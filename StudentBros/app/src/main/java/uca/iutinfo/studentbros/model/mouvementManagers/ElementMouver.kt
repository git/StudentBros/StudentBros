package uca.iutinfo.studentbros.model.mouvementManagers

import uca.iutinfo.studentbros.model.GameLoop
import uca.iutinfo.studentbros.model.gameManagers.GameManager
import uca.iutinfo.studentbros.model.gameManagers.MouvementManager
import kotlin.concurrent.thread

class ElementMouver(val mouvementManager: MouvementManager) : GameManager {
    override fun onChanged(t: GameLoop?) {
        thread {
            if (t != null){
                mouvementManager.moveAllElements(t.level)
            }
        }
    }
}