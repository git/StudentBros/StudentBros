package uca.iutinfo.studentbros.model.powerups

import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Side

abstract class PlayerThrowableElement(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float, velY: Float, isImmortal: Boolean, isVisible: Boolean, assets: Map<String, String>, selectedAsset: String, dangerousSides: List<Side>, gravityAffects: Boolean) : ThrowableElement(right, left, top, bottom, parent, velX, velY, isVisible, isImmortal, assets, selectedAsset, dangerousSides, gravityAffects) {
}