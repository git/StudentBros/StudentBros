package uca.iutinfo.studentbros.views

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uca.iutinfo.studentbros.R
import uca.iutinfo.studentbros.activity.LevelChoiceActivity
import uca.iutinfo.studentbros.model.Level

class AdapterLevelChoice(context: LevelChoiceActivity, arrayList: ArrayList<Level>) : RecyclerView.Adapter<AdapterLevelChoice.ViewHolder>() {
    private val context: Context
    private val arrayList: ArrayList<Level>
    var single_item_position = -1
    var levelSelected: Int = -1

    init {
        this.context = context
        this.arrayList = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_level, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val level = arrayList[position]
        holder.itemView.findViewById<TextView>(R.id.toto).text = level.name
        if (single_item_position == position) {
            holder.itemView.setBackgroundColor(Color.BLUE)
            levelSelected = level.numero //le numero d'un niveau est unique
            println(levelSelected)
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    inner class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { setSingleSelection(adapterPosition) }
        }
    }

    fun getLevelSelectedd() : Int {
        return levelSelected
    }

    private fun setSingleSelection(adapterPosition: Int) {
        if (adapterPosition == RecyclerView.NO_POSITION) return
        notifyItemChanged(single_item_position)
        single_item_position = adapterPosition
        notifyItemChanged(single_item_position)
    }
}




