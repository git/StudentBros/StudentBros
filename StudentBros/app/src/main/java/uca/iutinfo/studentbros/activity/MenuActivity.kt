package uca.iutinfo.studentbros.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import uca.iutinfo.studentbros.R

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        setContentView(R.layout.layout_menu)

        findViewById<Button>(R.id.buttonMenu).setOnClickListener {
            val intent = Intent(applicationContext,LevelChoiceActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.buttonClassement).setOnClickListener {
            //val intent = Intent(applicationContext,ClassementActivity::class.java)
            //startActivity(intent)
        }
    }
}