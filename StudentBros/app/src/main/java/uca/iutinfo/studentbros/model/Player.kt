package uca.iutinfo.studentbros.model

import uca.iutinfo.studentbros.model.powerups.PlayerThrowableElement

class Player(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float = 0F, velY: Float=80F, isImmortal: Boolean = false, isVisible: Boolean = true, vies: Int=1, var powerUp: PowerUp = StandardPowerUp(0f,0f, parent, 0f,0f)) : Character(right, left, top, bottom, parent, velX, velY, isImmortal, isVisible, mapOf("StandardPowerUpRight" to "Player/StandardPowerUpRight.png", "StandardPowerUpLeft" to "Player/StandardPowerUpLeft.png", "StandardPowerUpRunningLeft" to "Player/StandardPowerUpRunningLeft.png", "StandardPowerUpRunningRight" to "Player/StandardPowerUpRunningRight.png"), "Player/StandardPowerUpRight.png", mutableListOf(Side.DOWN), gravityAffects=true, vies) {

    private var run = 1

    fun pickPowerUp(p: PowerUp){
        powerUp = p
        powerUp.isVisible=false

    }

    fun usePower(side: Side, screenWidthCoef: Float, screenHeightCoef: Float) {
        powerUp.doSomething(side, screenWidthCoef, screenHeightCoef, this)
    }

    override fun jump() {
        this.velY = 90F
    }

    override fun bounce() {
        this.velY = 50F
    }

    override fun increaseDownVelocity() {
        this.velY =-30F
    }

    override fun increaseLeftVelocity() {
        this.velX = -30F
        if (run==1){
            selectedAsset = assets.filter { it.key == powerUp.name + "Left" }.values.first()
            run = 2
        }
        else{
            selectedAsset = assets.filter { it.key == powerUp.name + "RunningLeft" }.values.first()
            run = 1
        }
    }

    override fun increaseRightVelocity() {
        this.velX = 30F
        if (run == 1){
            selectedAsset = assets.filter { it.key == powerUp.name + "Right" }.values.first()
            run = 2
        }
        else{
            selectedAsset = assets.filter { it.key == powerUp.name + "RunningRight" }.values.first()
            run = 1
        }
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        this.powerUp.onHit(this, e)
    }

    override fun mettreUnCoup(e: Element) {
        //e.prendreUnCoup(this)
    }

    override fun mourir() {
        isVisible = false
        vies=0
        notifyEnd()
    }

    override fun randomAction() {
    }

    override fun actionOnCollision(e: Element, s: Side) {
        if(e.dangerousSides.any{ it.inverse() == s } && e !is PlayerThrowableElement){
            if(!isImmortal) prendreUnCoup(e, s)
        } else {
            e.prendreUnCoup(this, s.inverse())
        }
    }

    fun notifyEnd(){
        mObservers.forEach { it.notreOnChanged(false) }
    }

    fun addObserver(e: ElementObserver){
        mObservers.add(e)
    }
}