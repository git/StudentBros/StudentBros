package uca.iutinfo.studentbros.model.powerups

import android.util.Log
import uca.iutinfo.studentbros.model.*
import kotlin.concurrent.thread

class FireFlower(left: Float, bottom: Float, parent: Level, velX: Float, velY: Float) : PowerUp(left+100, left, bottom+100, bottom, parent, velX, velY, false, true, emptyMap(), "PowerUps/Banana.png", listOf(), "StandardPowerUp") {

    private var canDoSomething = true

    override fun doSomething(side: Side, screenWidthCoef: Float, screenHeightCoef: Float, player: Player) {
        if (canDoSomething){
            if (side==Side.RIGHT){
                val fire = FireBall(player.right+(20*screenWidthCoef), player.right, player.top-(player.top-player.bottom)/2+(10*screenHeightCoef), player.top-(player.top-player.bottom)/2-(10*screenHeightCoef), parent, 0F, 0F)
                fire.increaseRightVelocity()
                parent.addElement(fire)
                thread {
                    Thread.sleep(5000)
                    fire.mourir()
                }
            }
            if (side==Side.LEFT){
                val fire = FireBall(player.left, player.left-(20*screenWidthCoef), player.top-(player.top-player.bottom)/2+(10*screenHeightCoef), player.top-(player.top-player.bottom)/2-(10*screenHeightCoef), parent, 0F, 0F)
                fire.increaseLeftVelocity()
                parent.addElement(fire)
                thread {
                    Thread.sleep(5000)
                    fire.mourir()
                }
            }
            canDoSomething=false
            thread {
                Thread.sleep(1000)
                canDoSomething=true
            }
        }

    }

    override fun lost(p: Player) {
    }

    override fun onHit(p: Player, e: Element) {
        if (!p.isImmortal){
            p.vies--
            if(p.vies == 0) {
                p.mourir()
                return
            }
            else{
                p.powerUp = StandardPowerUp(0F,0F, parent, 0F, 0F)
            }
            p.isImmortal = true
            thread(start=true) {
                Thread.sleep(1000)
                p.isImmortal = false
            }
        }
    }

    override fun actionOnCollision(e: Element, s: Side) {
        if(e is Player){
            e.pickPowerUp(this)
            e.vies++
            mourir()
        }
    }

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if(e is Player){
            e.pickPowerUp(this)
            e.vies++
            mourir()
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
        isVisible=false
    }

    override fun randomAction() {
    }
}