package uca.iutinfo.studentbros.model

class MovingSideBlock(right: Float, left: Float, bottom: Float, top: Float, parent: Level, private val mouvementSize: Float, velX: Float=0F, velY: Float=0F) : Block(right, left, top, bottom, parent, velX, velY, emptyMap(), "Blocks/FlyingBlocks.png", listOf()) {

    var mouvementDone : Float = 0F

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
        velX = -10F
    }

    override fun increaseRightVelocity() {
        velX = 10F
    }

    override fun prendreUnCoup(e: Element, side: Side) {
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
        isVisible=false
    }

    override fun randomAction() {
        if (mouvementDone==mouvementSize){
            mouvementDone=0F
            if (velX>=0) increaseLeftVelocity()
            else increaseRightVelocity()
        }
        mouvementDone += velX
    }

    override fun actionOnCollision(e: Element, s: Side) {
    }
}