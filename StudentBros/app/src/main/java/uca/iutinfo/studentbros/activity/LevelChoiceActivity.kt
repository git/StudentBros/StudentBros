package uca.iutinfo.studentbros.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uca.iutinfo.studentbros.R
import uca.iutinfo.studentbros.data.Stub
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.views.AdapterLevelChoice


class LevelChoiceActivity : AppCompatActivity(){

     var level: Level? = null
    val adp = AdapterLevelChoice(this, Stub().lesLevels() as ArrayList<Level>)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.level_choice_layout)
        findViewById<Button>(R.id.buttonLevel).setOnClickListener {
            val selected = adp.getLevelSelectedd()
            val intent = Intent(applicationContext, GameActivity::class.java)
            intent.putExtra("key", selected)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        val lev = Stub().lesLevels()
        findViewById<RecyclerView>(R.id.recyclerViewLevelChoice).adapter = adp
        findViewById<RecyclerView>(R.id.recyclerViewLevelChoice).layoutManager = LinearLayoutManager(this)
    }

}