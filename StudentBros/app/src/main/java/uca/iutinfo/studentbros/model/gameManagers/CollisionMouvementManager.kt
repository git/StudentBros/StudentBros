package uca.iutinfo.studentbros.model.gameManagers

import uca.iutinfo.studentbros.model.*
import kotlin.concurrent.thread

class CollisionMouvementManager(val screenSize: Float) : MouvementManager {

    private val collisionManager: CollisionManager = ConcreteCollisionManager()

    override fun moveAllElements(level: Level) {
        level.elements.filterNot { it is Player || !it.isVisible }.forEach { moveE(it, level, it.velX, it.velY) }
    }

    override fun moveE(element: Element, level: Level, x: Float, y: Float): Boolean {
        val pair = collisionManager.distanceToCollision(element, level, x, y)
        element.top += pair.second
        element.bottom += pair.second
        element.right += pair.first
        element.left += pair.first
        return true
    }

    override fun moveP(player: Player, level: Level, x: Float, y: Float): Boolean {
        val pair = collisionManager.distanceToCollision(player, level, x, y)
        val screenCenter= screenSize/2F
        if (player.right<=screenCenter){
            var x = pair.first
            if (player.right + pair.first > screenCenter){
                x = screenCenter - player.right
                level.elements.filterNot { it is Player || !it.isVisible }.forEach{ moveEFromPlayer(it, level, -x)}
            }
            else if (player.left + pair.first < 0){
                x = -player.left
            }
            player.top += pair.second
            player.bottom += pair.second
            player.right += pair.first
            player.left += pair.first
        }
        return true
    }

    override fun movePlayer(player: Player, level: Level, side: Side, screenWidhtCoef: Float, screenHeightCoef: Float) {
        when(side){
            Side.LEFT -> moveP(player, level, -40F, 0F)
            Side.RIGHT -> moveP(player, level, 40F, 0F)
            Side.UP -> moveP(player, level, 0F, 40F)
            Side.DOWN -> moveP(player, level, 0F, -40F)
        }
    }

    override fun moveEFromPlayer(element: Element, level: Level, x: Float) {
        element.right += x
        element.left += x
    }
}