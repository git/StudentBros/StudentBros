package uca.iutinfo.studentbros.model

abstract class Block(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float, velY: Float, assets: Map<String, String>, selectedAssets: String, dangerousSide: List<Side>) : Element(right, left, top, bottom, parent, velX, velY, isImmortal = true, isVisible = true, assets, selectedAssets, dangerousSide, false)