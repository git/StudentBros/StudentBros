package uca.iutinfo.studentbros.model

enum class Side {
    LEFT,
    RIGHT,
    UP,
    DOWN
}

fun Side.inverse() : Side{
    return when(this){
        Side.LEFT -> Side.RIGHT
        Side.RIGHT -> Side.LEFT
        Side.UP -> Side.DOWN
        Side.DOWN -> Side.UP
    }
}