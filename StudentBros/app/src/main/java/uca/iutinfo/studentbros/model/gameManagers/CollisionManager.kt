package uca.iutinfo.studentbros.model.gameManagers

import uca.iutinfo.studentbros.model.Element
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Side

interface CollisionManager {
    fun detectCollision(element: Element, level: Level, x:Float, y: Float): Boolean

    fun distanceToCollision(element: Element, level: Level, x:Float, y: Float): Pair<Float, Float>

    fun collisionAction(element: Element, elementHitted: Element, collisionSide: Side)

    fun getCollisionSide(element: Element, elementHitted: Element, pair: Pair<Float, Float>): Side

    fun Element.intersects(element: Element): Boolean = left <= element.right && right >= element.left && top >= element.bottom && bottom <= element.top
    fun Element.intersectsWithVelocity(element: Element, x: Float, y: Float): Boolean = left + x < element.right && right + x > element.left && top + y > element.bottom && bottom + y < element.top
    fun getCollisionDistances(element: Element, elementHitted: Element, pair: Pair<Float, Float>): Pair<Float, Float>
    fun touchingGrass(element: Element, level: Level): Boolean
    fun directCollision(element: Element, level: Level, side: Side): Boolean
}