package uca.iutinfo.studentbros.model

class ConcreteBlock(right: Float, left: Float, top: Float, bottom: Float, parent: Level) : Block(right, left, top, bottom, parent, 0F, 0F, emptyMap(), "Blocks/GroundBlocks.png", listOf()) {
    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }

    override fun prendreUnCoup(e: Element, side: Side) {
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
    }

    override fun randomAction() {
    }

    override fun actionOnCollision(e: Element, s: Side) {
    }
}