package uca.iutinfo.studentbros.model.mouvementManagers

import uca.iutinfo.studentbros.model.GameLoop
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Player
import uca.iutinfo.studentbros.model.gameManagers.MouvementManager
import kotlin.concurrent.thread

class ConcreteElementActionManager(val mouvementManager: MouvementManager) : ElementActionManager {

    override fun actionAllElements(level: Level, screenSize: Float) {
        level.elements.filterNot { it is Player || !it.isVisible || it.left>screenSize || it.right<0 }.forEach {it.randomAction() }
    }

    override fun onChanged(t: GameLoop?) {
        if (t!=null){
            thread {
                actionAllElements(t.level, t.screenWidth)
            }
        }

    }
}