package uca.iutinfo.studentbros.data

import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.blocks.BlockWithPowerUp
import uca.iutinfo.studentbros.model.powerups.FireFlower

class  Stub : IDataManager {


    fun lesLevels() : MutableList<Level>{
        val lesLevels = mutableListOf<Level>()
        lesLevels.add(getLevel(1))
        lesLevels.add(getLevel(2))
        lesLevels.add(getLevel(3))
        lesLevels.add(getLevel(4))
        lesLevels.add(getLevel(5))
        lesLevels.add(getLevel(6))
        lesLevels.add(getLevel(7))
        lesLevels.add(getLevel(8))
        return lesLevels
    }

    override fun getScore(): RealUser {
        TODO("Not yet implemented")
    }


    fun getPowerUp(): List<PowerUp> {
        TODO("Not yet implemented")
    }

    fun getEnemy(): Enemy {
        TODO("Not yet implemented")
        /*
        val nemesis = Stickers, level1,)
        return nemesis
        */
    }

    override fun getPlayer(): Player {
        TODO("Not yet implemented")
        /*
        val thePowerOfLove = StandardPowerUp false, false, mapOf("pow-standard" to "lien pow-standard"), "lien pow-standard", "standard")
        /*return Player(14400f,14300f,700f,600f,0f,80f, false, true, 1, thePowerOfLove)*/
        return Player(400f,300f,700f,600f,0f,80f, false, true, 1, thePowerOfLove)
         */
    }
    override fun getLevel(niveau : Int?): Level {


        if(niveau == 1) {
            val level1 = Level(1, "La Plaine des Cézeaux", mutableListOf())
            //top max visible 1450
            //right : 2400
            //sol
            level1.addElement(
                ConcreteBlock(
                    6600F,00F,400F,0F,level1
                )
            )
            level1.addElement(
                Stickers(
                    800F, 400F, level1, 0F, 0F
                )
            )
            level1.addElement(
                ConcreteBlock(
                    8300F,6900F,400F,0F,level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    15500F,8700F,400F,0F,level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    25000F,15800F,400F,0F,level1
                )
            )

            //blocks 1
            level1.addElement(
                ConcreteBlock(
                    2000F, 1900F, 900F, 800F, level1
                )
            )
            //rangée de 5
            level1.addElement(
                ConcreteBlock(
                    2800F, 2300F, 900F, 800F, level1
                )
            ) //block au dessus de la rangée
            level1.addElement(
                ConcreteBlock(
                    2600F, 2500F, 1300F, 1200F, level1
                )
            ) //tuyeau 1
            level1.addElement(
                ConcreteBlock(
                    3300F, 3100F, 600F, 400F, level1
                )
            )

            //tuyeau 2
            level1.addElement(
                ConcreteBlock(
                    4100F, 3900F, 800F, 400F, level1
                )
            ) //tuyeau 3
            level1.addElement(
                ConcreteBlock(
                    4800F, 4600F, 900F, 400F, level1
                )
            )
            level1.addElement(
                Stickers(
                    5400F, 400F, level1, 0F, 0F
                )
            )
            //tuyeau 4
            level1.addElement(
                ConcreteBlock(
                    5800F, 5600F, 900F, 400F, level1
                )
            ) // 3 blocks entre 2 trous
            level1.addElement(
                ConcreteBlock(
                    7800F, 7500F, 900F, 800F, level1
                )
            ) // rangée haute au dessus du trou
            level1.addElement(
                ConcreteBlock(
                    8600F, 7800F, 1300F, 1200F, level1
                )
            ) //rangée haute à coté du trou
            level1.addElement(
                ConcreteBlock(
                    9300F, 8900F, 1300F, 1200F, level1
                )
            ) //block en dessous
            level1.addElement(
                ConcreteBlock(
                    9300F, 9200F, 900F, 800F, level1
                )
            ) //deux block avant triangle block
            level1.addElement(
                ConcreteBlock(
                    10000F, 9800F, 900F, 800F, level1
                )
            ) //block triangle le plus à gauche
            level1.addElement(
                ConcreteBlock(
                    10600F, 10500F, 900F, 800F, level1
                )
            )//block triangle milieu
            level1.addElement(
                ConcreteBlock(
                    10900F, 10800F, 900F, 800F, level1
                )
            ) // block le plus à droite
            level1.addElement(
                ConcreteBlock(
                    11200F, 11100F, 900F, 800F, level1
                )
            )//block triangle milieu haut
            level1.addElement(
                BlockWithPowerUp(
                    10800F, 1200F, level1, FireFlower(10900F, 10800F, level1, 0F, 0F)
                )
            )//block solo a droite
            level1.addElement(
                ConcreteBlock(
                    11900F, 11800F, 900F, 800F, level1
                )
            )//rangée de 3 haute à droite du solo
            level1.addElement(
                ConcreteBlock(
                    12400F, 12100F, 1300F, 1200F, level1
                )
            )//rangée de 4 haute antonoir
            level1.addElement(
                ConcreteBlock(
                    13200F, 12800F, 1300F, 1200F, level1
                )
            )//rangée de 2 basses antonoir
            level1.addElement(
                ConcreteBlock(
                    13100F, 12900F, 900F, 800F, level1
                )
            )
            level1.addElement(
                Stickers(
                    13300F, 400F, level1, -20F, 0F
                )
            )
            level1.addElement(
                Stickers(
                    13000F, 400F, level1, -20F, 0F
                )
            )
            level1.addElement(
                Stickers(
                    12600F, 400F, level1, -20F, 0F
                )
            )
            // 1er escalier qui monte : 1er ligne du bas
            level1.addElement(
                ConcreteBlock(
                    14000F, 13600F, 500F, 400F, level1
                )
            ) // 1er escalier qui monte : 2em ligne
            level1.addElement(
                ConcreteBlock(
                    14000F, 13700F, 600F, 400F, level1
                )
            ) // 1er escalier qui monte : 3em ligne
            level1.addElement(
                ConcreteBlock(
                    14000F, 13800F, 700F, 400F, level1
                )
            ) // 1er escalier qui monte : 4em ligne du haut
            level1.addElement(
                ConcreteBlock(
                    14000F, 13900F, 800F, 400F, level1
                )
            ) //1er escalier qui descend : ligne du bas
            level1.addElement(
                ConcreteBlock(
                    14600F, 14200F, 500F, 400F, level1
                )
            ) //1er escalier qui descend : 2e ligne
            level1.addElement(
                ConcreteBlock(
                    14500F, 14200F, 600F, 400F, level1
                )
            ) //1er escalier qui descend : 3e ligne
            level1.addElement(
                ConcreteBlock(
                    14400F, 14200F, 600F, 400F, level1
                )
            )//1er escalier qui descend : ligne du haut
            level1.addElement(
                ConcreteBlock(
                    14300F, 14200F, 800F, 400F, level1
                )
            ) //2em escalier qui monte : ligne du bas
            level1.addElement(
                ConcreteBlock(
                    15500F, 15000F, 500F, 400F, level1
                )
            ) //2ecs m : 2e ligne
            level1.addElement(
                ConcreteBlock(
                    15500F, 15100F, 600F, 400F, level1
                )
            ) //2ecs m : 3e ligne
            level1.addElement(
                ConcreteBlock(
                    15500F, 15200F, 700F, 400F, level1
                )
            )//2ecs m : ligne du haut
            level1.addElement(
                ConcreteBlock(
                    15500F, 15300F, 800F, 400F, level1
                )
            )//2ecs d : ligne du bas
            level1.addElement(
                ConcreteBlock(
                    16200F, 15800F, 500F, 400F, level1
                )
            )//2ecs d : 2e ligne
            level1.addElement(
                ConcreteBlock(
                    16100F, 15800F, 600F, 400F, level1
                )
            )//2ecs d : 3e ligne
            level1.addElement(
                ConcreteBlock(
                    16000F, 15800F, 700F, 400F, level1
                )
            )//2ecs d :  ligne du haut
            level1.addElement(
                ConcreteBlock(
                    15900F, 15800F, 800F, 400F, level1
                )
            ) //tuyeau apres escaliers
            level1.addElement(
                ConcreteBlock(
                    16800F, 16600F, 600F, 400F, level1
                )
            ) //derniere rangée de block
            level1.addElement(
                ConcreteBlock(
                    17600F, 17200F, 800F, 700F, level1
                )
            ) //dernier tuyeau
            level1.addElement(
                ConcreteBlock(
                    18600F, 18400F, 600F, 400F, level1
                )
            ) //escalier final, rangée du bas, chaque ligne représente une rangée en montant
            level1.addElement(
                ConcreteBlock(
                    19600F, 18600F, 500F, 400F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 18700F, 600F, 500F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 18800F, 700F, 600F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 18900F, 600F, 500F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 19000F, 900F, 800F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 19100F, 1000F, 900F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 19200F, 1100F, 1000F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 19300F, 1200F, 1100F, level1
                )
            )
            level1.addElement(
                ConcreteBlock(
                    19600F, 19400F, 1300F, 1200F, level1
                )
            ) //block de drapeau
            level1.addElement(
                ConcreteBlock(
                    20500F, 20400F, 500F, 400F, level1
                )
            ) //Drapeau
            level1.addElement(
                EndFlag(
                    20490F, 20410F, 1700F, 500F, level1
                )
            )
            return level1
        }
        if(niveau == 2) {
            val level2= Level(2,"Les Marécages du Restaurant Universitaire", mutableListOf())
            level2.addElement(
                ConcreteBlock(
                    2000F,00F,600F,0F,level2,
                )
            )
            level2.addElement(
                BlockWithPowerUp(
                    1500F, 1000F, level2, FireFlower(110F, 500F, level2, 0F, 0F)
                )
            )
            level2.addElement(ConcreteBlock(2700F,2500F,400F,300F,level2))

            level2.addElement(
                ConcreteBlock(3500F,3000F,400F,0F,level2)
            )
            level2.addElement(FallingBlock(
                4000F,3800F,300F,400F, level2

            )) // il est pas très falling celui la
            level2.addElement(FallingBlock(4500F,4300F,300F,400F, level2))

            level2.addElement(
                ConcreteBlock(7000F,4800F,400F,0F,level2)
            )

            level2.addElement(ConcreteBlock(7000F,6900F,600F,400F,level2))

            level2.addElement(Stickers(5000F,400F, level2,00F,0F))

            level2.addElement(Stickers(6000F,400F, level2,-20F,0F))

            level2.addElement(ConcreteBlock(7800F,7400F,400F,300F,level2))

            level2.addElement( SpikyBlock(
                8100F,7900F,300F,500F, level2))

            level2.addElement(ConcreteBlock(8600F,8300F,400F,300F,level2))

            level2.addElement( SpikyBlock(
                8900F,8700F,300F,500F, level2))

            level2.addElement(ConcreteBlock(9300F,9100F,400F,300F,level2))

            level2.addElement(ConcreteBlock(9900F,9500F,700F,00F,level2))

            level2.addElement( SpikyBlock(
                9900F,9500F,1300F,1500F, level2))

            level2.addElement(ConcreteBlock(15000F,9900F,400F,00F,level2))

            level2.addElement(Stickers(10000F,400F, level2,0F,0F))
            level2.addElement(Stickers(11000F,400F, level2,0F,0F))

            level2.addElement( SpikyBlock(
                12300F,12000F,400F,600F, level2))
            level2.addElement(Stickers(12500F,400F, level2,0F,0F))

            level2.addElement(ConcreteBlock(15000F,14500F,800F,00F,level2))

            level2.addElement(EndFlag(
                15000F, 14950F, 1800F, 600F, level2,

                ))


            return level2
        }
        if(niveau == 3) {
            val level3= Level(3,"Les labyrinthes de la machine a café", mutableListOf())
            level3.addElement(
                ConcreteBlock(
                    1500F,00F,600F,0F,level3))

            level3.addElement(
                ConcreteBlock(
                    2300F,2000F,500F,0F,level3))

            level3.addElement(
                ConcreteBlock(
                    3000F,2300F,1000F,0F,level3))
            level3.addElement(SpikyBlock(
                2700F,2600F,1000F,1200F, level3))
            level3.addElement(
                ConcreteBlock(
                    3900F,3400F,1000F,950F,level3))

            level3.addElement(
                ConcreteBlock(
                    4600F,4300F,800F,750F,level3))

            level3.addElement(
                ConcreteBlock(
                    5300F,5000F,500F,450F,level3))

            level3.addElement(BlockWithPowerUp(
                5150F, 700F, level3, FireFlower(110F, 500F, level3, 0F, 0F)
            ))

            level3.addElement(
                ConcreteBlock(
                    6000F,5600F,600F,00F,level3))
            level3.addElement(
                ConcreteBlock(
                    7000F,6000F,500F,00F,level3))

            level3.addElement(Stickers(6300F,500F, level3,-20F,0F))

            level3.addElement(
                ConcreteBlock(
                    7100F,7000F,800F,00F,level3))

            level3.addElement(
                ConcreteBlock(
                    7300F,7100F,1000F,00F,level3))

            level3.addElement(SpikyBlock(
                7600F,7400F,1000F,1200F, level3))

            level3.addElement(
                ConcreteBlock(
                    8800F,7300F,500F,00F,level3))

            level3.addElement(Stickers(8500F,500F, level3,-20F,0F))

            level3.addElement(
                EndFlag(
                    8800F, 8700F, 1800F, 500F, level3))

            return level3
        }

        if(niveau == 4) {

            val level4 = Level(4, "La descente de l'amphithéatre", mutableListOf())


            level4.addElement(
                ConcreteBlock(
                    2000F,00F,400F,0F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    3500F,3000F,400F,0F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    3000F,2000F,400F,350F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    22000F,19000F,400F,0F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    1000F,800F,600F,0F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    1600F,1500F,800F,700F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    1600F,1500F,800F,700F,level4,
                )
            )

            level4.addElement(
                ConcreteBlock(
                    4200F,3800F,400F,350F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    4900F,4500F,500F,450F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    5600F,5200F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    6200F,5900F,600F,00F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    6900F,6200F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    7200F,6900F,600F,000F,level4,
                )
            )
            level4.addElement( //a remplacer par un enemy
                ConcreteBlock(
                    6700F,6500F,800F,600F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    7800F,6900F,2000F,900F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    8400F,8100F,700F,650F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    8400F,7200F,50F,00F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    8100F,7800F,400F,350F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    7800F,7500F,900F,350F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    8700F,8400F,700F,000F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    9500F,9100F,700F,000F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    11300F,9900F,700F,000F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock( // a remplacer par un monstre
                    10800F,10600F,900F,700F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock( //
                    11800F,11600F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock( //
                    12300F,12100F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock( //
                    12800F,12600F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock( //
                    13300F,13100F,600F,550F,level4,
                )
            )
            level4.addElement(
                ConcreteBlock(
                    18000F,13600F,600F,00F,level4,
                )
            )
            level4.addElement(
                EndFlag(
                    15500F, 15420F, 1800F, 600F, level4,

                )
            )
            return level4

        }
        if(niveau == 5) {
            return Level(5, "Le zoo réseaux télécommunications", mutableListOf())
        }
        if(niveau == 6) {
            val level6 = Level(6, "Les catacombes sinistres de l'IUT", mutableListOf())
            level6.addElement(
                    ConcreteBlock(
                        20000F,00F,1500F,1400F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        2700F,00F,400F,0F,level6

                    )
                )
                level6.addElement(
                    Stickers(2000F,500F, level6,-20F,0F)
                )

                level6.addElement(
                    ConcreteBlock(
                        5000F,3100F,400F,0F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        2700F,2400F,700F,0F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        3400F,3100F,700F,0F, level6
                    )
                )

                level6.addElement(
                    Stickers(5700F,1001F,level6, 20F, 0F)
                )
                level6.addElement(
                    Stickers(4500F,401F,level6, 20F, 0F)
                )



                level6.addElement(
                    FallingBlock(
                        4800F,4600F,650F,700F, level6

                    )
                )


                level6.addElement(
                    ConcreteBlock(
                        5800F,5000F,1000F,0F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        6500F,6200F,500F,400F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        7100F,6800F,500F,400F, level6

                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        7700F,7400F,500F,400F, level6

                    )
                )

                level6.addElement(
                    ConcreteBlock(
                        9000F,8000F,900F,0F, level6

                    )
                )
            level6.addElement(
                Stickers(9500F,401F,level6, 20F, 0F)
            )
            level6.addElement(
                Stickers(10000F,401F,level6, 20F, 0F)
            )
                level6.addElement(
                    ConcreteBlock(
                        10600F,9000F,400F,0F, level6

                    )
                )

                level6.addElement(
                    FallingBlock(
                        11000F,10800F,600F,650F, level6
                    )
                )
                level6.addElement(
                    ConcreteBlock(
                        11800F,11200F,900F,00F, level6

                    )
                )
            level6.addElement(
                SpikyBlock(
                    12000F,11800F,700F,900F, level6

                )
            )
            level6.addElement(
                ConcreteBlock(
                    12500F,12000F,900F,00F, level6

                )
            )
            level6.addElement(
                ConcreteBlock(
                    15000F,12700F,400F,0F, level6

                )
            )

            level6.addElement(
                SpikyBlock(
                    13500F,13400F,400F,600F, level6
                    )
            )


            level6.addElement(Stickers(13700F,401F,level6, 20F, 0F)
            )

            level6.addElement(
                SpikyBlock(
                    14100F,14000F,400F,700F, level6
                    )
            )

            level6.addElement(
                FallingBlock(
                    15500F,15300F,600F,650F, level6
                )
            )
            level6.addElement(
                FallingBlock(
                    16000F,15800F,600F,650F, level6
                )
            )
            level6.addElement(
                FallingBlock(
                    16500F,16300F,600F,650F, level6
                )
            )

            level6.addElement(
                ConcreteBlock(
                    17500F,16800F,800F,0F, level6

                )
            )
            level6.addElement(
                ConcreteBlock(
                    20000F,16800F,400F,0F, level6
                )
            )
            level6.addElement(
                ConcreteBlock(
                    18800F,17800F,1400F,700F,level6
                )
            )
            level6.addElement(
                FallingBlock(
                    18800F,17500F,0F,800F, level6
                )
            )
            level6.addElement(
                EndFlag(19500F,19400F,1300F,400F,level6)
            )
            //
            return level6
        }
        if(niveau == 7) {
            return Level(7, "L'affreuse salle MP003", mutableListOf())
        }
        if(niveau == 8) {
            return Level(8, "L'ascention finale de l'étage C", mutableListOf())
        }
        else{
            return Level(0,"Niveau bonus : VDN", mutableListOf())
        }



    }



}