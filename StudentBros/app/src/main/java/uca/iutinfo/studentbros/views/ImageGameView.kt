package uca.iutinfo.studentbros.views
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.MotionEvent
import io.socket.client.Socket
import uca.iutinfo.studentbros.activity.EndGameActivity
import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.gameManagers.ConcreteCollisionManager
import uca.iutinfo.studentbros.model.mouvementManagers.ConcreteElementActionManager
import uca.iutinfo.studentbros.model.mouvementManagers.ElementMouver
import uca.iutinfo.studentbros.model.mouvementManagers.InertieMouvementManager
import uca.iutinfo.studentbros.multiplayer.SocketHandler
import kotlin.concurrent.thread

class ImageGameView(context: Context, level: Level, multiplayer: Boolean) : GameView(context,level, multiplayer), ElementObserver{

    private var running = true
    private var rightPressed = Pair(false, 0)
    private var leftPressed = Pair(false, 0)
    private var jumpPressed = Pair(false, 0)
    private var powerUpPressed = Pair(false, 0)

    private lateinit var bitmap: Bitmap
    private lateinit var canvas: Canvas


    private var mario = Player(400f,300f,775f,600f, level,0f,80f, false, true, 1, StandardPowerUp(0F,0F, level, 0F, 0F))

    private var buttonLeft = RectF()
    private var buttonRight = RectF()
    private var buttonJump = RectF()
    private var buttonPowerUp = RectF()


    private var rectMap = mutableMapOf<Element, RectF>()
    private val mouvementManager = InertieMouvementManager(height.toFloat(), ConcreteCollisionManager())
    private val gameLoop = GameLoop(level, height.toFloat(), width.toFloat(), 0F)

    private val elementActionManager = ConcreteElementActionManager(mouvementManager)
    private var screenHeightCoef = 0F
    private var screenWidthCoef = 0F

    private val elementMouver = ElementMouver(mouvementManager)

    private var time=0F

    private lateinit var mSocket: Socket

    private var otherPlayer = Player(mario.right, mario.left, mario.top, mario.bottom, powerUp = mario.powerUp, parent = Level(12, "", mutableListOf()))

    init {

        level.elements.add(mario)
        gameLoop.addObserver(mouvementManager.inertieManager)
        gameLoop.addObserver(mouvementManager.gravityManager)
        gameLoop.addObserver(elementActionManager)
        gameLoop.addObserver(elementMouver)

        initGame()
        if (multiplayer){
            initMultiplayer()
        }
    }

    override fun stop() {
        running = false
        gameLoop.running = false
    }

    override fun restart(){
        running=true
        buttonThread()
        gameLoop.running = true
        gameLoop.gameThread()
        timeThread()
    }

    private fun initGame(){
        thread {
            Thread.sleep(1000)
            screenHeightCoef=width/1440F
            screenWidthCoef=height/2112F

            gameLoop.screenWidth = height.toFloat()

            mario.addObserver(this)
            mario.addObserver(gameLoop)
            level.elements.forEach { if (it is EndFlag){ it.addObserver(this); it.addObserver(gameLoop) }}

            buttonLeft=RectF(50F*screenHeightCoef, 50F*screenWidthCoef, 325F*screenHeightCoef, 400F*screenWidthCoef)
            buttonRight = RectF(50F*screenHeightCoef, 450F*screenWidthCoef, 325F*screenHeightCoef, 800F*screenWidthCoef)
            buttonPowerUp = RectF( 50F*screenHeightCoef, 1350F*screenWidthCoef, 325F*screenHeightCoef, 1700*screenWidthCoef)
            buttonJump = RectF( 50F*screenHeightCoef, 1750F*screenWidthCoef, 325F*screenHeightCoef, 2100F*screenWidthCoef)

            level.changeSize(screenWidthCoef, screenHeightCoef)

            level.elements.forEach { rectMap.put(it, RectF(it.bottom, it.left,it.top,it.right))}

            mouvementManager.screenSize=height.toFloat()
            gameLoop.screenWidthCoef=screenWidthCoef
            gameLoop.screenHeightCoef=screenHeightCoef

        }
    }

    private fun initMultiplayer(){
        thread {
            SocketHandler.setSocket()
            mSocket = SocketHandler.getSocket()
            mSocket.connect()
            mSocket.emit("join", 1)
            multiplayerThread(mSocket)

            Log.d("hey", mSocket.connected().toString())
            mSocket.on("test"){
                Log.d("hey", "blablabla")
            }

            mSocket.on("opponentMove"){ arg ->

                if (arg.size == 4){
                    val r = (arg[0] as Number).toFloat()*screenWidthCoef
                    val l = (arg[1] as Number).toFloat()*screenWidthCoef
                    val t = (arg[2] as Number).toFloat()*screenHeightCoef
                    val b = (arg[3] as Number).toFloat()*screenHeightCoef
                    val levelEnd = level.getEndPos()
                    if (levelEnd != null){
                        otherPlayer.right = levelEnd - r
                        otherPlayer.left = levelEnd - l
                        otherPlayer.top = t
                        otherPlayer.bottom = b

                    }
                }
            }
        }
    }

    private fun multiplayerThread(mSocket: Socket){
        thread{
            while (running) {
                val levelEnd = level.getEndPos()
                if (levelEnd != null){
                    val right = (levelEnd-mario.right)/screenWidthCoef
                    val left = (levelEnd-mario.left)/screenWidthCoef
                    val top = mario.top/screenHeightCoef
                    val bot = mario.bottom/screenHeightCoef
                    mSocket.emit("updatePos", 1, right.toInt(), left.toInt(), top.toInt(), bot.toInt())
                }
                Thread.sleep(60)
            }
        }
    }

    private val paintOtherPlayer = Paint().apply {
        color=Color.TRANSPARENT
        style=Paint.Style.FILL
        strokeWidth=4F
    }

    private val paintButton = Paint().apply {
        color=Color.RED
        style=Paint.Style.FILL
        strokeWidth=4F
    }

    private val paintText = Paint().apply {
        color=Color.BLACK
        style=Paint.Style.FILL
        textSize=30F
        strokeWidth=4F
    }

    private val paint = Paint()

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        canvas = Canvas(bitmap)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas : Canvas?){
        canvas?.apply {
            val originalBitmap1 = BitmapFactory.decodeStream(context.assets.open("Background.png"))
            val matrix = Matrix()
            matrix.postRotate(90f)
            val rotatedBitmap1 = Bitmap.createBitmap(originalBitmap1, 0, 0, originalBitmap1.width, originalBitmap1.height, matrix, true)
            canvas.drawBitmap(rotatedBitmap1, null, RectF(0F, 0F, width.toFloat(), height.toFloat()), paint)

            level.elements.filter { it.left<height && it.isVisible && it.right>0 }.forEach {
                val originalBitmap = BitmapFactory.decodeStream(context.assets.open(it.selectedAsset))
                val rotatedBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.width, originalBitmap.height, matrix, true)
                if (it is Character){
                    val rect = RectF(it.bottom, it.left, it.top, it.right)
                    canvas.drawBitmap(rotatedBitmap, null, rect, paint)
                }
                else {
                    if (rotatedBitmap.height < it.right - it.left) {
                        var n = it.left
                        while (n < it.right) {
                            var d = rotatedBitmap.height
                            if (n + rotatedBitmap.height > it.right) {
                                d = (it.right - n).toInt()
                            }
                            val rect = RectF(it.bottom, n, it.top, n + d)
                            canvas.drawBitmap(rotatedBitmap, null, rect, paint)
                            n += rotatedBitmap.height
                        }

                    } else {
                        val rect = RectF(it.bottom, it.left, it.top, it.right)
                        canvas.drawBitmap(rotatedBitmap, null, rect, paint)
                    }
                }
            }

            drawRect(buttonLeft, paintButton)
            drawRect(buttonRight, paintButton)
            drawRect(buttonJump, paintButton)
            drawRect(buttonPowerUp, paintButton)
            drawText((time/1000F).toString(), 1000F, 1000F, paintText)
            if (mutliplayer){
                drawRect(RectF(otherPlayer.top, otherPlayer.right,otherPlayer.bottom,otherPlayer.left), paintOtherPlayer)
            }

        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event!=null){
            val pointerIndex = event.actionIndex
            val pointerId = event.getPointerId(pointerIndex)
            val maskedAction = event.actionMasked

            when (maskedAction) {
                MotionEvent.ACTION_DOWN ->{
                    if (buttonLeft.contains(event.getX(pointerIndex), event.getY(pointerIndex))) {
                        leftPressed = Pair(true, 1)
                        mouvementManager.movePlayer(mario, level, Side.LEFT,  1F, 1F)
                    }
                    if (buttonRight.contains(event.getX(pointerIndex), event.getY(pointerIndex))) {
                        rightPressed = Pair(true, 1)
                        mouvementManager.movePlayer(mario, level, Side.RIGHT,  1F, 1F)
                    }
                    if (buttonJump.contains(event.getX(pointerIndex), event.getY(pointerIndex))){
                        jumpPressed = Pair(true, 1)
                        mouvementManager.movePlayer(mario, level, Side.UP, 1F, 1F)
                    }
                    if (buttonPowerUp.contains(event.getX(pointerIndex), event.getY(pointerIndex))){
                        powerUpPressed = Pair(true, 1)
                    }
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                    if (buttonLeft.contains(event.getX(pointerIndex), event.getY(pointerIndex)) && !leftPressed.first && !rightPressed.first){
                        leftPressed = Pair(true, 2)
                        mouvementManager.movePlayer(mario, level, Side.LEFT,  1F, 1F)
                    }
                    if (buttonRight.contains(event.getX(pointerIndex), event.getY(pointerIndex)) && !leftPressed.first && !rightPressed.first){
                        rightPressed = Pair(true, 2)
                        mouvementManager.movePlayer(mario, level, Side.RIGHT,  1F, 1F)
                    }
                    if (buttonJump.contains(event.getX(pointerIndex), event.getY(pointerIndex)) && !jumpPressed.first){
                        jumpPressed = Pair(true, 2)
                        mouvementManager.movePlayer(mario, level, Side.UP, 1F, 1F)
                    }
                    if (buttonPowerUp.contains(event.getX(pointerIndex), event.getY(pointerIndex)) && !powerUpPressed.first){
                        powerUpPressed = Pair(true, 2)
                    }
                }
                MotionEvent.ACTION_UP -> {
                    if (leftPressed.second==1) leftPressed = Pair(false, 0)
                    if (rightPressed.second==1) rightPressed = Pair(false, 0)
                    if (jumpPressed.second==1) jumpPressed = Pair(false, 0)
                    if (powerUpPressed.second==1) powerUpPressed = Pair(false, 0)

                }
                MotionEvent.ACTION_POINTER_UP -> {
                    if (leftPressed.second==2) leftPressed = Pair(false, 0)
                    if (rightPressed.second==2) rightPressed = Pair(false, 0)
                    if (jumpPressed.second==2) jumpPressed = Pair(false, 0)
                    if (powerUpPressed.second==2) powerUpPressed = Pair(false, 0)
                }
            }
            invalidate()
            return true
        }
        return false
    }

    private fun buttonThread() {
        thread{
            while (running){
                if (leftPressed.first) mouvementManager.movePlayer(mario, level, Side.LEFT, 1F, 1F)
                if (rightPressed.first) mouvementManager.movePlayer(mario, level, Side.RIGHT, 1F, 1F)
                if (jumpPressed.first) mouvementManager.movePlayer(mario, level, Side.UP, 1F, 1F)
                if (powerUpPressed.first){
                    if (mario.velX>=0) mario.usePower(Side.RIGHT, screenWidthCoef, screenHeightCoef)
                    else mario.usePower(Side.LEFT, screenWidthCoef, screenHeightCoef)
                }
                Thread.sleep(10)
            }
        }
    }

    private fun timeThread(){
        thread{
            while (running){
                time += 60F
                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable { // Update UI code here
                    invalidate()
                })
                Thread.sleep(60)
            }
        }
    }

    override fun notreOnChanged(victoire: Boolean) {
        running = false

        val intent = Intent(context, EndGameActivity::class.java)
        intent.putExtra("isWon", victoire)
        intent.putExtra("level", level.numero)
        intent.putExtra("time", time)
        context.startActivity(intent)

    }

}

