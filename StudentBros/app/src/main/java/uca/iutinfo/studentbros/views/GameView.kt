package uca.iutinfo.studentbros.views

import android.content.Context
import android.view.View
import uca.iutinfo.studentbros.model.Level

abstract class GameView(context: Context,val level: Level, val mutliplayer: Boolean) : View(context) {

    abstract fun stop()

    abstract fun restart()
}