package uca.iutinfo.studentbros.model

import android.database.Observable

abstract class Element(var right: Float, var left: Float, var top: Float, var bottom: Float, val parent: Level, var velX: Float= 0F, var velY: Float = 0F, var isImmortal: Boolean = false, var isVisible: Boolean = true, val assets: Map<String, String>, var selectedAsset: String, val dangerousSides: List<Side>, val gravityAffects: Boolean) : Observable<ElementObserver>(), java.io.Serializable {
    abstract fun actionOnCollision(e: Element, s: Side)

    abstract fun jump()

    abstract fun bounce()

    abstract fun increaseDownVelocity()

    abstract fun increaseLeftVelocity()

    abstract fun increaseRightVelocity()

    abstract fun prendreUnCoup(e: Element, side: Side)

    abstract fun mettreUnCoup(e: Element)

    abstract fun mourir()

    abstract fun randomAction()
}