package uca.iutinfo.studentbros.model

import kotlin.concurrent.thread

class FallingBlock(right: Float, left: Float, bottom: Float, top: Float, parent: Level, velX: Float=0F, velY: Float=0F) : Block(right, left, top, bottom, parent, velX, velY, emptyMap(), "Blocks/FallingBlocks.png", listOf()){

    var touched = false

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }
    override fun prendreUnCoup(e: Element, side: Side) {
        if (e is Player && !touched && side == Side.UP){
            touched=true
            thread {
                Thread.sleep(1500)
                mourir()
            }
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
        isVisible=false
    }

    override fun randomAction() {
    }

    override fun actionOnCollision(e: Element, s: Side) {
    }

}