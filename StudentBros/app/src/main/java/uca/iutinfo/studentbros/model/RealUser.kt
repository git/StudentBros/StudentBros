package uca.iutinfo.studentbros.model
import java.util.Date



class RealUser(username: String, scores: Map<Level, Date>) : User(username, scores) {
    val score = scores.toMutableMap()
    private val username = username

    override fun addScores(level : Level, date : Date){
        score.put(level,date)
    }

    override fun getScoreByLevel(level: Level): Date {
        return score.getValue(level)
    }

}

