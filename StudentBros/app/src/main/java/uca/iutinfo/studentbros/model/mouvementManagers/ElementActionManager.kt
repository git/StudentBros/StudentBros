package uca.iutinfo.studentbros.model.mouvementManagers

import androidx.lifecycle.Observer
import uca.iutinfo.studentbros.model.GameLoop
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.gameManagers.GameManager

interface ElementActionManager : GameManager {

    fun actionAllElements(level: Level, screenSize: Float)
}