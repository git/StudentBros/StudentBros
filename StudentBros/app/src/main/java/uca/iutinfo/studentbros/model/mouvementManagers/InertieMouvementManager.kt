package uca.iutinfo.studentbros.model.mouvementManagers

import uca.iutinfo.studentbros.model.Element
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Player
import uca.iutinfo.studentbros.model.Side
import uca.iutinfo.studentbros.model.gameManagers.ConcreteCollisionManager
import uca.iutinfo.studentbros.model.gameManagers.MouvementManager

class InertieMouvementManager(var screenSize: Float, val collisionManager: ConcreteCollisionManager) :
    MouvementManager {

    val gravityManager = ConcreteGravityManager(this, collisionManager)
    val inertieManager = ConcreteInertieManager(this)

    override fun moveAllElements(level: Level) {
        level.elements.filter { it !is Player && (it.velX != 0F || it.velY != 0F) && it.isVisible && it.right>-screenSize/3 && it.left<screenSize+screenSize/3}.forEach {
            if (it.velX != 0F ) inertieManager.moveElement(it, level)
            if (it.velY != 0F ) gravityManager.jump(it, level)
        }
    }
    override fun moveE(element: Element, level: Level, x: Float, y: Float): Boolean {
        var side = if (x>0) Side.RIGHT
        else if (y>0) Side.UP
        else if (y<0) Side.DOWN
        else Side.LEFT
        if (collisionManager.directCollision(element, level, side)) return false
        val pair = collisionManager.distanceToCollision(element, level, x, y)
        element.top += pair.second
        element.bottom += pair.second
        element.right += pair.first
        element.left += pair.first
        return pair.first == x && pair.second == y
    }

    override fun moveP(player: Player, level: Level, leftRight: Float, upDown: Float): Boolean {
        var side = if (leftRight>0) Side.RIGHT
        else if (upDown>0) Side.UP
        else if (upDown<0) Side.DOWN
        else Side.LEFT
        if (collisionManager.directCollision(player, level, side)) return false
        val screenCenter = screenSize/2F
        val pair = collisionManager.distanceToCollision(player, level, leftRight, upDown)
        var x = pair.first
        if (player.right==screenCenter && x>0F){
            level.elements.filterNot { it is Player || !it.isVisible }.forEach{ moveEFromPlayer(it, level, -x)}
            x=0F
        }
        else if (player.right + x > screenCenter){
            x = screenCenter - player.right
            level.elements.filterNot { it is Player || !it.isVisible }.forEach{ moveEFromPlayer(it, level, -x)}
        }
        else if (player.left + pair.first < 0){
            x = -player.left
        }
        player.top += pair.second
        player.bottom += pair.second
        player.right += x
        player.left += x
        return pair.first==leftRight && pair.second == upDown
    }

    override fun movePlayer(player: Player, level: Level, side: Side, screenWidhtCoef: Float, screenHeightCoef: Float) {
        when(side){
            Side.LEFT -> {
                player.increaseLeftVelocity()
                player.velX *= screenWidhtCoef
                inertieManager.moveElement(player, level)
            }
            Side.RIGHT -> {
                player.increaseRightVelocity()
                player.velX *= screenWidhtCoef
                inertieManager.moveElement(player, level)
            }
            Side.UP -> {
                if (collisionManager.touchingGrass(player, level)) {
                    player.jump()
                    gravityManager.jump(player, level)
                }
            }
            else -> return
        }
    }

    override fun moveEFromPlayer(element: Element, level: Level, x: Float) {
        element.right += x
        element.left += x
    }
}