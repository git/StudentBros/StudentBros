package uca.iutinfo.studentbros.model.mouvementManagers

import androidx.lifecycle.Observer
import uca.iutinfo.studentbros.model.*
import uca.iutinfo.studentbros.model.gameManagers.GameManager

interface GravityManager : GameManager, Observer<GameLoop> {
    fun jump(element: Element, level: Level)

    fun makeThemFall(level: Level)

}