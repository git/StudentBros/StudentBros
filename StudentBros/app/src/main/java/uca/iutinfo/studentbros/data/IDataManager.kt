package uca.iutinfo.studentbros.data

import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Player
import uca.iutinfo.studentbros.model.RealUser


interface IDataManager {

    fun getLevel(niveau : Int?): Level

    fun getPlayer(): Player

    fun getScore() : RealUser
}