package uca.iutinfo.studentbros.model.blocks

import uca.iutinfo.studentbros.model.*
import kotlin.concurrent.thread

class BlockWithPowerUp(left: Float, bottom: Float, parent: Level, val powerUp: PowerUp) : Block(left+100, left, bottom+100, bottom, parent, 0F, 0F, emptyMap(), "Blocks/BlocksWithPower.png", listOf()) {

    private var touched = false

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
    }

    override fun increaseRightVelocity() {
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if (e is Player && side == Side.DOWN && !touched){
            powerUp.bottom=top
            powerUp.left=left
            powerUp.right=right
            powerUp.top=top+100
            parent.addElement(powerUp)
            touched = true
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
    }

    override fun randomAction() {
    }

    override fun actionOnCollision(e: Element, s: Side) {
    }

}