package uca.iutinfo.studentbros.activity

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import uca.iutinfo.studentbros.data.Stub
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.multiplayer.SocketHandler
import uca.iutinfo.studentbros.views.GameView
import uca.iutinfo.studentbros.views.ImageGameView
import uca.iutinfo.studentbros.views.RectangleGameView

class GameActivity: AppCompatActivity() {

    private lateinit var level : Level
    private lateinit var gameView: GameView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val extra= intent.extras
        if(extra !=null) {
            val lev = extra.getInt("key")
            level = Stub().getLevel(lev)
            gameView = ImageGameView(this, level, false)
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            setContentView(gameView)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        var multiplayer = false
        if (multiplayer){
            SocketHandler.getSocket().disconnect()
        }
    }

    override fun onStop() {
        super.onStop()
        gameView.stop()
    }

    override fun onStart() {
        super.onStart()
        gameView.restart()
    }


}