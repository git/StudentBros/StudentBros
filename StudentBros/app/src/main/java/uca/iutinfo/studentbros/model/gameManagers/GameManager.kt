package uca.iutinfo.studentbros.model.gameManagers

import androidx.lifecycle.Observer
import uca.iutinfo.studentbros.model.GameLoop

interface GameManager: Observer<GameLoop> {

    override fun onChanged(t: GameLoop?)
}