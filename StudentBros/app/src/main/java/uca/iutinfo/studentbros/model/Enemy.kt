package uca.iutinfo.studentbros.model

abstract class Enemy(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float, velY: Float,  isImmortal: Boolean, isVisible: Boolean, assets: Map<String, String>, selectedAsset: String, dangerousSides: List<Side>, gravityAffects: Boolean,vies: Int) : Character(right, left, top, bottom, parent, velX, velY, isImmortal, isVisible, assets, selectedAsset, dangerousSides, gravityAffects, vies)  {
}