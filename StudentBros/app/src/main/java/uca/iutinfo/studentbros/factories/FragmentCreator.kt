package uca.iutinfo.studentbros.factories

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory

class fragment_creator : FragmentFactory() {


    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        when(className){
            /*
            death_fragment::class.java.name -> death_fragment()
            game_fragment::class.java.name -> game_fragment()
            leaderboard_fragment::class.java.name -> leaderboard_fragment()
            level_menu_fragment::class.java.name -> level_menu_fragment()
            menu_fragment::class.java.name -> menu_fragment()
            Pause_fragment::class.java.name -> Pause_fragment()
            win_fragment::class.java.name -> win_fragment()
            else -> menu_fragment()

             */
        }
        return super.instantiate(classLoader, className)
    }
}