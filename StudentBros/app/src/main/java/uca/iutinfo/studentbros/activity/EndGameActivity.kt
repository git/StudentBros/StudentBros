package uca.iutinfo.studentbros.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import uca.iutinfo.studentbros.R

class EndGameActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val extra = intent.extras
        if (extra != null) {
            val isWon = extra.getBoolean("isWon")
            setContentView(R.layout.layout_endgame)
            if (isWon) {
                var time = intent.getFloatExtra("time", 0F)
                time /= 1000
                findViewById<TextView>(R.id.textFin).text =
                    "${getString(R.string.victoire)} : $time"
            } else {
                findViewById<TextView>(R.id.textFin).text = getString(R.string.defaite)
            }

            findViewById<Button>(R.id.buttonRejouer).setOnClickListener {
                val extra = intent.extras
                val selected = extra?.getInt("level")
                val intent = Intent(applicationContext, GameActivity::class.java)
                intent.putExtra("key", selected)
                startActivity(intent)
            }
            findViewById<Button>(R.id.buttonToMenu).setOnClickListener {
                val intent = Intent(applicationContext, MenuActivity::class.java)
                startActivity(intent)
            }
        }
    }
}