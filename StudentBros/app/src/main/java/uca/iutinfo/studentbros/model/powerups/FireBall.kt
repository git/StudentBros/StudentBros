package uca.iutinfo.studentbros.model.powerups

import uca.iutinfo.studentbros.model.Element
import uca.iutinfo.studentbros.model.Level
import uca.iutinfo.studentbros.model.Player
import uca.iutinfo.studentbros.model.Side

class FireBall(right: Float, left: Float, top: Float, bottom: Float, parent: Level, velX: Float, velY: Float) : PlayerThrowableElement(right, left, top, bottom, parent, velX, velY, true, true, emptyMap(), "ThrowableElements/Bubble.png", listOf(Side.UP, Side.DOWN, Side.LEFT, Side.RIGHT), false) {

    override fun actionOnCollision(e: Element, s: Side) {
        if (e is ThrowableElement) {
            mourir()
            return
        }
        if (e !is Player){
            e.prendreUnCoup(this, s)
            mourir()
        }
    }

    override fun jump() {
    }

    override fun bounce() {
    }

    override fun increaseDownVelocity() {
    }

    override fun increaseLeftVelocity() {
        velX=-50F
    }

    override fun increaseRightVelocity() {
        velX=50F
    }

    override fun prendreUnCoup(e: Element, side: Side) {
        if (e is ThrowableElement) {
            mourir()
            return
        }
        if (e !is Player){
            e.prendreUnCoup(this, side)
            mourir()
        }
    }

    override fun mettreUnCoup(e: Element) {
    }

    override fun mourir() {
        isVisible=false
    }

    override fun randomAction() {
    }
}