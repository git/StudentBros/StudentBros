package uca.iutinfo.studentbros

/*
class CollisionMouvementManagerTest {

    private val stub = Stub()
    private val player = stub.getPlayer()
    private val level = stub.getLevel()
    private val mouvementManager = CollisionMouvementManager()

    @Test
    fun testMoveElementAccordingToPlayer(){
        player.right = 10F
        player.left= 0F
        player.top=20F
        player.bottom=10F
        player.velX = 20F
        player.velY = 0F
        level.elements[0].left = 20F
        level.elements[0].right = 40F
        level.elements[0].top = 4000F
        level.elements[0].bottom = 0F
        mouvementManager.moveElementAccordingToPlayer(level, player, 100F)

        assert(player.right==level.elements[0].left && player.bottom==10F)
    }

    @Test
    fun testMoveElementAccordingToPlayer2(){
        player.right = 10F
        player.left= 0F
        player.top=20F
        player.bottom=10F
        player.velX = 20F
        player.velY = 0F
        level.elements[0].left = 40F
        level.elements[0].right = 60F
        level.elements[0].top = 4000F
        level.elements[0].bottom = 0F
        mouvementManager.moveElementAccordingToPlayer(level, player, 100F)

        assert(player.right==30F && player.left==20F && player.bottom==10F)
    }

    @Test
    fun testMoveElementAccordingToPlayer3(){
        player.right = 30F
        player.left= 20F
        player.top=20F
        player.bottom=10F
        player.velX = -20F
        player.velY = 0F
        level.elements[0].left = 40F
        level.elements[0].right = 60F
        level.elements[0].top = 4000F
        level.elements[0].bottom = 0F
        mouvementManager.moveElementAccordingToPlayer(level, player, 100F)

        assert(player.right==10F && player.left==0F && player.bottom==10F)
    }

    @Test
    fun testMoveElementAccordingToPlayer4(){
        player.right = 20F
        player.left= 10F
        player.top=20F
        player.bottom=10F
        player.velX = -20F
        player.velY = 0F
        level.elements[0].left = 40F
        level.elements[0].right = 60F
        level.elements[0].top = 4000F
        level.elements[0].bottom = 0F
        mouvementManager.moveElementAccordingToPlayer(level, player, 100F)

        assert(player.right==10F && player.left==0F && player.bottom==10F)
    }
}

 */