package uca.iutinfo.studentbros

import org.junit.Test
import uca.iutinfo.studentbros.data.Stub
import uca.iutinfo.studentbros.model.gameManagers.SimpleMouvementManager

/*
class SimpleMouvementManagerTest {

    private val stub = Stub()
    private val player = stub.getPlayer()
    private val level = stub.getLevel()
    private val mouvementManager = SimpleMouvementManager()

    @Test
    fun testMoveElement(){
        val p=stub.getPlayer()
        player.increaseRightVelocity()
        player.jump()
        mouvementManager.moveElement(player, level, player.velX, player.velY)

        assert(player.right==p.right+player.velX && player.left==p.left+player.velX)
        assert(player.top==p.top+player.velY && player.bottom==p.bottom+player.velY)

    }

    @Test
    fun testMoveElementAccordingToPlayer(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        player.right=10F
        player.top=10F
        p.right=10F
        p.top=10F
        var i=0
        player.increaseRightVelocity()
        player.jump()
        //mouvementManager.moveElementAccordingToPlayer(level, player, 100F)

        assert(player.right==p.right+player.velX && player.left==p.left+player.velX)
        assert(player.top==p.top+player.velY && player.bottom==p.bottom+player.velY)
        level.elements.forEach {
            assert(it.right==l.elements[i].right && it.left==l.elements[i].left)
            ++i
        }
    }

    @Test
    fun testMoveElementAccordingToPlayer2(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        player.right=10F
        player.top=10F
        p.right=10F
        p.top=10F
        var i=0
        player.increaseRightVelocity()
        player.jump()
        //mouvementManager.moveElementAccordingToPlayer(level, player, 10F)

        assert(player.right==p.right && player.left==p.left)
        level.elements.forEach {
            assert(it.right==l.elements[i].right-player.velX && it.left==l.elements[i].left-player.velX)
            ++i
        }
    }

    @Test
    fun testMoveElementAccordingToPlayer3(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        player.right=10F
        player.top=10F
        player.velX=20F
        p.right=10F
        p.top=10F
        var i=0

        //mouvementManager.moveElementAccordingToPlayer(level, player, 20F)

        assert(player.right==p.right+10F && player.left==p.left+10F)
        level.elements.forEach {
            assert(it.right==l.elements[i].right-10F && it.left==l.elements[i].left-10F)
            ++i
        }
    }


    @Test
    fun testMoveElementAccordingToPlayer4(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        player.right=20F
        player.left=10F
        player.top=10F
        player.velX=-20F
        p.right=20F
        p.left=10F
        p.top=10F
        var i=0

        //mouvementManager.moveElementAccordingToPlayer(level, player, 50F)

        assert(player.right==p.right-10F && player.left==0F)
        level.elements.forEach {
            assert(it.right==l.elements[i].right-10F && it.left==l.elements[i].left-10F)
            ++i
        }
    }

    @Test
    fun testMoveElementAccordingToPlayer5(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        player.right=10F
        player.left=0F
        player.top=10F
        player.velX-20F
        p.left=0F
        p.right=10F
        p.top=10F
        var i=0

        //mouvementManager.moveElementAccordingToPlayer(level, player, 50F)

        assert(player.right==p.right && player.left==0F)
        level.elements.forEach {
            assert(it.right==l.elements[i].right && it.left==l.elements[i].left)
            ++i
        }
    }

    @Test
    fun testMoveAllElements(){
        val l = stub.getLevel()
        val p=stub.getPlayer()
        var i=0

        player.velX=10F
        level.elements[0].velX=20F

        mouvementManager.moveAllElements(level)
        assert(player.right==p.right && player.left==p.left)
        level.elements.forEach {
            assert(it.right==l.elements[i].right+it.velX && it.left==l.elements[i].left+it.velX)
            ++i
        }

    }
}

 */