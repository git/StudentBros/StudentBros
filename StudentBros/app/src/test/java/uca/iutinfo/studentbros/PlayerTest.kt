package uca.iutinfo.studentbros

import org.junit.Test
import uca.iutinfo.studentbros.data.Stub
import uca.iutinfo.studentbros.model.PowerUp
import kotlin.concurrent.thread


class PlayerTest {

    val stub = Stub()
    val powerUp = stub.getPowerUp()
    val player = stub.getPlayer()
    val toto = stub.getEnemy()

    @Test
    fun pickPowerUpTest(){
        player.velX = 0f
        player.pickPowerUp(powerUp.first())
        assert(player.powerUp == powerUp.first())
        assert(!powerUp.first().isVisible)
        assert(!player.powerUp.isVisible)
        assert(player.selectedAsset == "thePowerOfLove-right")
        player.velX = -2f
        player.pickPowerUp(powerUp.last())
        assert(player.powerUp == powerUp.last())
        assert(!powerUp.last().isVisible)
        assert(!player.powerUp.isVisible)
        assert(player.selectedAsset == "thePuckPower-right")
    }

    @Test
    fun prendreUnCoupTest(){
        player.prendreUnCoup(toto)
        assert(player.vies == 2)
        assert(player.isImmortal)
        thread(start=true) {
            Thread.sleep(1500L)
        }
        assert(!player.isImmortal)
        assert(player.isVisible)
        player.prendreUnCoup(toto)
        assert(player.isImmortal)
        thread(start=true) {
            Thread.sleep(1500L)
        }
        assert(!player.isImmortal)
        assert(player.vies == 1)
        assert(player.isVisible)
        player.prendreUnCoup(toto)
        assert(player.vies == 0)
        assert(!player.isVisible)
    }

    @Test
    fun disappearTest(){
        player.mourir()
        assert(!player.isVisible)
    }

    @Test
    fun mettreUnCoupTest(){
        player.mettreUnCoup(toto)
        assert(!toto.isVisible)
    }
}