const express = require('express')
const path = require('path')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var userList = [];
var groupList = [];

app.set('port', (process.env.PORT || 8080));


io.on("connection", socket => { 

  console.log(socket.id)

  io.emit("test");

  socket.on('join', (nb) => {
    console.log(socket.id, "joined")
    socket.join(nb);
  })

  socket.on('updatePos', (nb, right, left, up, down) => {
    socket.to(nb).emit('opponentMove', right, left, up, down)
  })

  socket.on('quit', () => {
    socket.disconnect();
  })


});

http.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
